const getSum = (str1, str2) => {
  if( typeof str1 === 'object' && typeof str2 === 'object' ||  isNaN(parseInt(str1)) &&  isNaN(parseInt(str2)))
  {
    return false;
  }
  if(str1.length == 0 )
  {
    return parseInt(str2).toString();
  }
  if(str2.length == 0 )
  {
    return parseInt(str1).toString();
  }

  return (parseInt(str1) + parseInt(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var a = 0;
  var b = 0;
 
  
 
  for(var i = 0; i < listOfPosts.length; i++)
  {
      if(listOfPosts[i].author ===  authorName)
      {
         a++;
                
      }
      if(listOfPosts[i].comments != undefined )
      {
    for(var j =0; j < listOfPosts[i].comments.length; j++)
        {
            if(listOfPosts[i].comments[j].author === authorName)
            {
                
                b++;
            }
        
        }
      }
  }
  

  
  return 'Post:' + a +',comments:' + b;
};

const tickets=(people)=> {
  
  var temp25 = 0;
  var temp50 = 0;
  var temp100 = 0;

  for(var i = 0; i < people.length; i++)
  {
    if(people[i] == 25)
    {
      temp25 ++;
    }
   




    if(people[i] == 50)
    {
      if(temp25 > 0)
      {
        temp25--;
        temp50++;
      }
      else
      {
        return 'NO';
      }
    }





    if(people[i] == 100)
    {
      if(temp50 > 0 && temp25 > 0)
      {
        temp50--;
        temp25--;
        temp100++;
        continue;
      }
      if(temp50 == 0 && temp25 > 2)
      {
        temp25--;
        temp25--;
        temp25--;
        temp100++;
      }
      else
      {
        return 'NO';
      }
    }
  }

  return 'YES';

};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
